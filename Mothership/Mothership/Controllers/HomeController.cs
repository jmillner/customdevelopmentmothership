﻿using Mothership.CustomAppRunner;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mothership.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult GetTestIndex()
		{
					
			CustomAppWrapper cappWrapper = new CustomAppWrapper();
			MemoryStream stream = new MemoryStream();

			// TODO generate the cappInfo from query params like current CAPPS
			// TODO - Check login token
			CappTaskInfo cappInfo = new CappTaskInfo();
			cappInfo.CappFolder = "./CAPP/";
			cappInfo.CompanyCode = "15050";
			cappInfo.CompanyCode = "JDM_Dev";
			cappInfo.Id = "12345_SpecialPriceCatWorker";
			cappInfo.UserID = "jmillner";
			cappInfo.CurrentCommand = "index";
			
			cappWrapper.CustomAppCommand(stream, cappInfo);
			var sr = new StreamReader(stream);
			string html = sr.ReadToEnd();
			ViewBag.html = html;

			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}