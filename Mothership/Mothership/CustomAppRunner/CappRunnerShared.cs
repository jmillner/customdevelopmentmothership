﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Web;

namespace Mothership.CustomAppRunner
{
	public static class CappRunnerShared
	{
		public const string CAPPS_FOLDER = "CompiledCapps";
		public const string MMF_NAME_PREFIX_INPUT = "CappRunnerInput-";
		public const string MMF_NAME_PREFIX_OUTPUT = "CappRunnerOutput-";
		public const string STARTUP_EVENT_PREFIX = "CappRunnerStartup-";
		public const string EXIT_EVENT_PREFIX = "CappRunnerExit-";
		public const string COMMAND_EVENT_PREFIX = "CappRunnerCommand-";
		public const string CLEANUP_EVENT_PREFIX = "CappRunnerCleanup-";
		public const string GC_EVENT_PREFIX = "CappRunnerGC-";
		public const string HEARTBEAT_MUTEX_PREFIX = "Global\\CappRunnerHeartbeat-";


		// TODO - Check to make sure we want these timeouts
		public const int TIMEOUT_SECONDS = 600; // 10 minutes
		public const int GC_TIMEOUT_SECONDS = 30;
		public const long MAX_RUNNER_MEMORY = 2L * 1024 * 1024 * 1024; // 2GB

		public static void WriteMemStreamToMMF(MemoryMappedFile mmf, MemoryStream stream)
		{
			if (stream.Length > int.MaxValue)
				throw new Exception("Data is too large to write to memory.");

			using (MemoryMappedViewAccessor accessor = mmf.CreateViewAccessor())
			{
				accessor.WriteArray(0, stream.ToArray(), 0, (int)stream.Length);
			}
		}

		public static MemoryStream ReadMMFToMemStream(MemoryMappedFile mmf)
		{
			using (MemoryMappedViewAccessor accessor = mmf.CreateViewAccessor())
			{
				byte[] buffer = new byte[accessor.Capacity];
				accessor.ReadArray(0, buffer, 0, buffer.Length);

				MemoryStream stream = new MemoryStream(buffer);
				stream.Write(buffer, 0, buffer.Length);
				stream.Position = 0;
				return stream;
			}
		}
	}

	[Serializable]
	public class CappTaskInfo
	{
		public string Id { get; set; }
		public string CompanyPin { get; set; }
		public string UserID { get; set; }
		public string CompanyCode { get; set; }
		public string CappFolder { get; set; }
		public string CurrentCommand { get; set; }
	}

	[Serializable]
	public class CappRunnerInputData
	{
		public string CappId { get; set; }
		public string CappFolder { get; set; }
		public string CommandName { get; set; }
		public string ApplicationPath { get; set; }
		public string AppVersion { get; set; }
		public string RequestID { get; set; }
		public CappUser CappUser { get; set; }
	}

	[Serializable]
	public class CappRunnerOutputData
	{
		public Exception Error { get; set; }
		public byte[] Data { get; set; }
	}

	[Serializable]
	public class CappUser
	{
		public int PinCode { get; set; }
		public string CompanyCode { get; set; }
		public string UserId { get; set; }
	}
}