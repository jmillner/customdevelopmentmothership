﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Web;

namespace Mothership.CustomAppRunner
{
	public class CustomAppProcess : IDisposable
	{
		public delegate void LogFunc(string text, bool isError);

		private Process process = null;
		private string appPath;
		private string appVersion;
		public string CappId;
		public string ProcessGUID;
		private LogFunc log;
		private Mutex heartbeatMutex = null;

		public Process Process { get { return process; } }

		public CustomAppProcess(string version, string path, string cappId, string processGUID, LogFunc logFunc)
		{
			this.log = logFunc;
			this.CappId = cappId;
			this.appPath = path;
			this.appVersion = version;
			this.ProcessGUID = processGUID;
		}
		public void Startup()
		{
			if (process == null)
			{
				if (heartbeatMutex != null)
				{
					heartbeatMutex.Close();
					heartbeatMutex.Dispose();
					heartbeatMutex = null;
				}

				log($"Starting new Report Runner Process {CappId}'....", false);
				// create heartbeat mutex 
				// it will be monitored by runner
				// when it goes away runner will know to exit

				heartbeatMutex = new Mutex(true, CappRunnerShared.HEARTBEAT_MUTEX_PREFIX + CappId + ProcessGUID);

				using (EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, CappRunnerShared.STARTUP_EVENT_PREFIX + CappId + ProcessGUID))
				{
					try
					{
						string cappProcessName = CappId;
						// split by spaces in the app start
						process = Process.Start(Path.Combine(appPath, CappRunnerShared.CAPPS_FOLDER, CappId, cappProcessName + ".exe"), CappId + " " + ProcessGUID);

						// now wait for process to signal us that it's ready
						if (!waitHandle.WaitOne(CappRunnerShared.TIMEOUT_SECONDS * 1000))
							throw new TimeoutException($"Timeout waiting for E2Capp process '{CappId}' (PID: {process.Id}) to signal ready.");

					}
					catch (Exception ex)
					{
						log($"E2Capp Process '{CappId}' failed to start: {ex.Message}", true);
						throw;
					}
				}
			}
			else
			{
				// if the prosses has shut itself down or had an error, log and recursively call startup()
				if (process.HasExited)
				{
					log($"WARNING: E2Capp Process '{CappId}' (PID: {process.Id}) doesn't seem to be running. Cleaning up...", false);

					if (heartbeatMutex != null)
					{
						heartbeatMutex.Close();
						heartbeatMutex.Dispose();
						heartbeatMutex = null;
					}

					process.Dispose();
					process = null;

					log($"E2Capp Process '{CappId}' restarting...", false);

					Startup();
				}
			}
		}

		// similar concept to the Export() of the ReportRunnerProcess
		public void RunCommand(MemoryStream stream, CappTaskInfo cappInfo, string command)
		{
			try
			{
				// create names for inputs and outputs
				string inputName = CappRunnerShared.MMF_NAME_PREFIX_INPUT + CappId + ProcessGUID;
				string requestID = Guid.NewGuid().ToString(); // used to ID the individual requsts for each capp command
				string outputName = CappRunnerShared.MMF_NAME_PREFIX_OUTPUT + CappId + ProcessGUID + requestID;

				CappRunnerInputData inputData = new CappRunnerInputData()
				{
					CappId = CappId,
					CappFolder = cappInfo.CappFolder,
					CommandName = command,
					ApplicationPath = appPath,
					AppVersion = appVersion,
					RequestID = requestID
				};

				// write input params to a MemoryMappedFile
				using (MemoryStream mmfStream = new MemoryStream())
				{
					IFormatter formatter = new BinaryFormatter();
					formatter.Serialize(mmfStream, inputData);

					using (MemoryMappedFile mmf = MemoryMappedFile.CreateNew(inputName, mmfStream.Length))
					{
						CappRunnerShared.WriteMemStreamToMMF(mmf, mmfStream);

						mmfStream.Close();

						// notify process that we want it to run a command - add the command to the Event
						using (EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, CappRunnerShared.COMMAND_EVENT_PREFIX + CappId + ProcessGUID))
						{
							// wait for the command to execute
							Stopwatch sw = Stopwatch.StartNew();
							// good ol' infinite loop
							while(true)
							{
								// wait for 3 seconds
								if (waitHandle.WaitOne(3000))
								{
									// export complete
									break; // out of the loop
								}
								else
								{
									// wait timed out but command has not executed yet
									if (sw.ElapsedMilliseconds > CappRunnerShared.TIMEOUT_SECONDS * 1000)
										throw new TimeoutException($"Timeout waiting for E2Capp process '{CappId}' (PID: {process.Id}) to signal command is ready.");
									else
									{
										// check the size of the process in case it's eating a ton of memory while running
										long privateBytes = TryGetMemorySize();
										if (privateBytes > CappRunnerShared.MAX_RUNNER_MEMORY)
										{
											// we exceeded allowed memory size
											// we must kill the process and report the error
											log($"E2Capp Process '{CappId}' (PID: {process.Id}): E2Capp exceeded allowed memory limit while running the command. Current size: {privateBytes / 1024 / 1024}MB. Killing the process dead...", true);
											process.Kill();
											log($"E2Capp Process '{CappId}' (PID: {process.Id}): PIN: {cappInfo.CompanyPin}, CompanyCode: {cappInfo.CompanyCode}, User: {cappInfo.UserID}, Command: '{command}'", true);

											// we don't care what happens next to the capp
											throw new Exception($"E2Capp process '{CappId}' (PID: {process.Id}) exceeded allowed memory limit. Is report too big?");
										}
									}
								}

							}
						}
					}
				}
				CappRunnerOutputData outData = null;
				//MemoryStream outStream;

				// read output
				using (MemoryMappedFile mmf = MemoryMappedFile.OpenExisting(outputName))
				using (MemoryStream outStream = CappRunnerShared.ReadMMFToMemStream(mmf))
				{
					IFormatter formatter = new BinaryFormatter();
					outData = (CappRunnerOutputData)formatter.Deserialize(outStream);
				}

				// signal runner that it can cleanup
				using (EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, CappRunnerShared.CLEANUP_EVENT_PREFIX + CappId + ProcessGUID))
					waitHandle.Set();

				if (outData != null)
				{
					if (outData.Error != null)
						throw outData.Error;
					else
					{
						//outStream = new MemoryStream(outData.Data);
						stream.Write(outData.Data, 0, outData.Data.Length);
						stream.Position = 0; // Reset position in stream to the beginning.
					}
				}
			}
			catch (Exception ex)
			{
				log($"E2Capp Runner '{CappId}' :" + ex.ToString(), true);
				throw ex;
			}

		}

		public void Shutdown(bool disposing)
		{
			if (process != null)
			{
				if (disposing)
					log($"E2Capp Process '{CappId}' (PID: {process.Id}) is being disposed...", false);
				else
					log($"E2Capp Process '{CappId}' (PID: {process.Id}) is being shutdown...", false);
			}

			if (process.HasExited)
			{
				log($"E2Capp Process '{CappId}' (PID: {process.Id}) is not running.", false);
				process.Dispose();
				process = null;
				return;
			}

			log($"E2Capp Process '{CappId}' (PID: {process.Id}): notifying E2Capp to exit...", false);

			// notify process that we want it to exit
			using (EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, CappRunnerShared.EXIT_EVENT_PREFIX + CappId + ProcessGUID))
			{
				// now wait for process to signal us that it's exiting
				log($"E2Capp Process '{CappId}' (PID: {process.Id}): waiting for E2Capp to aknowledge exit signal...", false);
				if (waitHandle.WaitOne(CappRunnerShared.TIMEOUT_SECONDS * 1000)) // if the process is generating report it might take up to 5 minutes to finish
				{
					log($"E2Capp Process '{CappId}' (PID: {process.Id}): E2Capp acknowledged exit signal.", false);
				}
				else
				{
					log($"E2Capp Process '{CappId}' (PID: {process.Id}): Timed out waiting for E2Capp to acknowledge exit.", true);
				}

				// wait for up to one minute
				log($"E2Capp Process '{CappId}' (PID: {process.Id}): waiting for process to exit...", false);
				if (process.WaitForExit(60000))
				{
					log($"E2Capp Process '{CappId}' (PID: {process.Id}): E2Capp exited with Exit Code {process.ExitCode}.", false);
				}
				else
				{
					log($"E2Capp Process '{CappId}' (PID: {process.Id}): E2Capp did not exit in one minute. Killing it...", true);
					process.Kill();
					if (!process.WaitForExit(60000))
					{
						log($"E2Capp Process '{CappId}' (PID: {process.Id}): E2Capp still did not exit. Giving up trying...", true);
					}
				}

				log($"E2Capp Process '{CappId}' (PID: {process.Id}): Cleaning up and exiting.", false);

				process.Dispose();
				process = null;
			}

			if (heartbeatMutex != null)
			{
				heartbeatMutex.Close();
				heartbeatMutex.Dispose();
				heartbeatMutex = null;
			}
		}

		public bool CollectGarbage()
		{
			if (process != null)
			{
				log($"E2Capp Process '{CappId}' (PID: {process.Id}): Requesting Garbage Collection...", false);

				if (process.HasExited)
				{
					log($"E2Capp Process '{CappId}' (PID: {process.Id}) is not running.", false);
					process.Dispose();
					process = null;
					return false;
				}

				log($"E2Capp Process '{CappId}' (PID: {process.Id}): notifying E2Capp to collect garbage...", false);

				// notify process that we want it to exit
				using (EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, CappRunnerShared.GC_EVENT_PREFIX + CappId + ProcessGUID))
				{
					// now wait for process to signal us that it's done collecting
					log($"E2Capp Process '{CappId}' (PID: {process.Id}): waiting for E2Capp to aknowledge GC signal...", false);
					if (waitHandle.WaitOne(CappRunnerShared.GC_TIMEOUT_SECONDS * 1000))
					{
						log($"E2Capp Process '{CappId}' (PID: {process.Id}): E2Capp acknowledged GC signal.", false);
						return true;
					}
					else
					{
						log($"E2Capp Process '{CappId}' (PID: {process.Id}): Timed out waiting for E2Capp to acknowledge GC.", true);
						return false;
					}
				}
			}
			else
				return false;
		}

		public long TryGetMemorySize()
		{
			long size = -1;
			if (process != null && !process.HasExited)
			{
				try
				{
					// must refresh to get current (not cached) values from process
					process.Refresh();
					size = process.PrivateMemorySize64;
				}
				catch (Exception ex)
				{
					log($"E2Capp Process '{CappId}' (PID: {process.Id}): Unable to get private memory size: {ex.Message}", true);
				}
			}
			return size;
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					Shutdown(true);
				}
				disposedValue = true;
			}
		}

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			Dispose(true);
		}
		#endregion
	}
}