﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Mothership.CustomAppRunner
{
	public class CustomAppWrapper
	{
		// JDM - This is heavily based on the ReportRunner archetecture so if something is broken... Ask KV.

		private const int MIN_MAX_RAM = 100;
		private const int MIN_RUNNERS = 1;
		private static long MaxRAM;

		// TODO - do we need a semaphore here?
		// only allow one thread at the time to enter initialization check section
		private static SemaphoreSlim initSemaphore = new SemaphoreSlim(1, 1);
		// Dictionary of currently running Capps
		// Key = CappID + Process GUID
		private static ConcurrentDictionary<string, Queue<CustomAppProcess>> liveCappDic;
		public static string AppVersion { get; set; }
		private static string AppPath = AppDomain.CurrentDomain.BaseDirectory;
		public CustomAppWrapper()
		{
			try
			{
				initSemaphore.Wait();

				// we haven't been initialized
				if (AppVersion == null)
					Initialize();
			}
			finally
			{
				initSemaphore.Release();
			}
		}

		public void Initialize()
		{
			// this pretty much gets called once per app when the first special is run
			// (per server)

			// ensure *special*Dir folder exists
			// TODO - fix path
			string path = Path.Combine(AppPath, CappRunnerShared.CAPPS_FOLDER);
			// ensure *special*Dir\Logs folder exists
			path = Path.Combine(path, "Logs");
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);
			Log("--------------------------------------------------------------------------------------", false);
			Log("Initializing CustomAppWrapper...", false);

			// todo appsettings
			int maxRAM = 300;//AppSettings.MaxRAM;
			if (maxRAM < MIN_MAX_RAM)
				maxRAM = MIN_MAX_RAM;

			MaxRAM = maxRAM * 1024 * 1024;

			AppVersion = "1.0"; // TODO - App versions?
			Log($"AppVersion is {AppVersion}.", false);

			liveCappDic = new ConcurrentDictionary<string, Queue<CustomAppProcess>>();
		}

		public void CustomAppCommand(MemoryStream stream, CappTaskInfo capp)
		{			
			CustomAppProcess cappProcess = null;
			
			try
			{
				// try and pull out an already running process to use
				cappProcess = tryDequeueCapp(capp.Id);


				if (cappProcess == null)
				{
					// spin up new process, add to the dictionary
					string processGUID = Guid.NewGuid().ToString();
					cappProcess = CreateProcess(capp.Id, processGUID);
					queueCapp(cappProcess);
				}

				// start it up, if it's already running this will do nothing
				cappProcess.Startup();


				// now that we have a process all to ourselves let's call Export on it
				cappProcess.RunCommand(stream, capp, capp.CurrentCommand);

			}
			catch (Exception ex)
			{
				// if command ended in error, we will shutdown the process and spawn a new one
				if (cappProcess != null)
				{
					// start a separate thread that will initiate shutdown
					// so we don't have to wait for it
					Task.Factory.StartNew((paramObj) =>
					{
						// wooo doggy
						CustomAppProcess localProcess = (CustomAppProcess)((object[])paramObj)[0];
						string errMessage = (string)((object[])paramObj)[1];
						int procID = 0;

						if (localProcess.Process != null)
							procID = localProcess.Process.Id;

						Log($"E2Capp Process '{localProcess.CappId}' (PID: {procID}) finished with error: {errMessage}", false);

						// before attempting the shutdown
						// make sure we add a new process into the queue
						// don't start it here to make sure it is not picked up by someone while it's starting
						string processGUID = Guid.NewGuid().ToString();
						CustomAppProcess newCapp = CreateProcess(capp.Id, processGUID);
						queueCapp(newCapp);

						// initiate shutdown
						localProcess.Shutdown(false);
						Log($"E2Capp Process '{localProcess.CappId}' (PID: {procID}) has been recycled.", false);
						localProcess.Dispose();
						localProcess = null;

					}, new object[] { cappProcess, ex.ToString() });
				}
				throw;
			}
			finally
			{
				if (cappProcess != null)
				{
					// start a separate thread that will check process size 
					// and will attemp to GC or shutdown as needed
					// or if everything is well it will just return it into the queue
					Task.Factory.StartNew((inProcess) =>
					{
						CustomAppProcess localProcess = (CustomAppProcess)inProcess;

						long privateBytes = localProcess.TryGetMemorySize();
						if (privateBytes >= MaxRAM)
						{
							Log($"E2Capp Process '{localProcess.CappId}' (PID: {localProcess.Process.Id}) exceeded allowed RAM. Private Bytes {privateBytes / 1024 / 1024}MB.", false);
							if (localProcess.CollectGarbage())
							{
								// check new size after GC
								privateBytes = localProcess.TryGetMemorySize();
								if (privateBytes > -1)
									Log($"E2Capp Process '{localProcess.CappId}' (PID: {localProcess.Process.Id}): New size after GC is {privateBytes / 1024 / 1024}MB.", false);
							}
						}
						if (privateBytes == -1 || privateBytes >= MaxRAM)
						{
							if (privateBytes > -1)
								Log($"E2Capp Process '{localProcess.CappId}' (PID: {localProcess.Process.Id}) still exceeding allowed RAM. Recycling...", false);
							else
								Log($"E2CappProcess '{localProcess.CappId}' (PID: {localProcess.Process.Id}) memory size couldn't be determined. Recycling (just in case)...", false);

							// before attempting the shutdown
							// make sure we add a new process into the queue
							// don't start it here to make sure it is not picked up by someone while it's starting
							string processGUID = Guid.NewGuid().ToString();
							CustomAppProcess newCapp = CreateProcess(capp.Id, processGUID);
							queueCapp(newCapp);

							// initiate shutdown
							localProcess.Shutdown(false);
							Log($"E2Capp Process '{localProcess.CappId}' (PID: {localProcess.Process.Id}) has been recycled.", false);
							localProcess.Dispose();
							localProcess = null;
						}
					}, cappProcess);
				}
			}
		}

		private static CustomAppProcess CreateProcess(string cappId, string processGUID)
		{	
			CustomAppProcess process = new CustomAppProcess(AppVersion, AppPath, cappId, processGUID, Log);
			return process;
		}

		private static void queueCapp(CustomAppProcess cappProcess) {
			// check if ConcurentDictionary already has a queue for this capp

			liveCappDic.TryGetValue(cappProcess.CappId, out Queue<CustomAppProcess> cappQueue);
			if (cappQueue == null)
			{
				// create a new queue for the CDic
				cappQueue = new Queue<CustomAppProcess>();
			}

			cappQueue.Enqueue(cappProcess);

			liveCappDic["key"] = cappQueue;
		}

		private CustomAppProcess tryDequeueCapp(string cappId)
		{
			CustomAppProcess cappProcess = null;
			liveCappDic.TryGetValue(cappId, out Queue<CustomAppProcess> cappQueue);
			if (cappQueue == null)
			{
				// we have no queue for this capp, therefore no running capps, return null
				// the queue will be created in queueCap()
				return null;
			}

			// queue found, lets pull one out to use and return
			cappProcess = cappQueue.Dequeue();
			// update the queue in the dictionary
			liveCappDic[cappId] = cappQueue;
			return cappProcess;
		}


		// TODO - Check if we need loglocks
		private static object logLock = new object();
		public static void Log(string text, bool isError)
		{
			// one at a time please
			lock (logLock)
			{
				string time = DateTime.Now.ToString("hh:mm:ss.fff tt") + ": ";
				string logText = (isError ? "ERROR: " : "") + text;

				Debug.WriteLine(time + logText);
				WriteLog(time, logText);
			}
		}

		private static void WriteLog(string time, string text)
		{
			try
			{
				// TODO - check that this works
				string filename = "CustomAppWrapper_" + DateTime.Now.ToString("yyyyMMdd") + ".log";
				using (StreamWriter writer = new StreamWriter(Path.Combine(AppPath, CappRunnerShared.CAPPS_FOLDER, "Logs", filename), true))
				{
					writer.WriteLine(time + text);
					writer.Close();
				}
			}
			catch (Exception ex)
			{
			}
		}
	}
}